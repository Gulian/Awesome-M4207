#include <ESP8266WiFi.h>

void setup() {
 Serial.begin(9600);
}

void loop() {
    LWiFi.begin();   // On active le wifi
    LWiFi.connect("Kampus");
    delay(10000);
    printCurrentNet();
     LWiFi.end();     // on éteins le wifi

}



void printCurrentNet() {
  // on afficher le SSID sur lequel on est connecté
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // On affiche l'adresse MAC de l'AP sur lequel on est connecté
  byte bssid[6];
  LWiFi.BSSID(bssid);    
  Serial.print("BSSID: ");
  
  for (int compteur =0; compteur<5; compteur++){  //on affiche "un a un" chaque partie de l'adresse MAC de l'AP 
    Serial.print(bssid[compteur],HEX);
    Serial.print(":");
  }
  
  Serial.println(bssid[5],HEX); //on affiche la dernière partie de l'adresse MAC avec un retour à la ligne

  // on affiche la puissance du signal de l'AP
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

}

