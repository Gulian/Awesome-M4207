#include <LBT.h> 
#include <LBTServer.h>
int ledPin = 4;  //Pin the LED is connected to on the LinkIt ONE
#define BUZZER_PIN               8            /* sig pin of the buzzer */
#define LED 4
int i =0;

String command;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
LBTServer.begin((uint8_t*)"STARPLA");
pinMode(LED,OUTPUT);
digitalWrite(LED,LOW);
pinMode(BUZZER_PIN, OUTPUT); /* set buzzer pin as output */
digitalWrite(BUZZER_PIN,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:

if(LBTServer.connected())
{
  Serial.println("Connected");  //If bluetooth is connected print "Connected" in serial port
  }
else
{
  Serial.println("Retrying");   //Else retry
  LBTServer.accept(5);
  }

command = LBTServer.readString();

if (command == "on"){
  digitalWrite(LED,HIGH);   //If command is "On", switch LED on
  Serial.println("LED is on");
  LBTServer.write("LED is on");

  for (i = 0; i < 3; i++) { //If command is "On", switch BUZZER on for 3 times
        digitalWrite(BUZZER_PIN, HIGH);
        delay(500);
        digitalWrite(BUZZER_PIN, LOW);
        delay(1000);
    }
  Serial.println("BUZZ is on");
  LBTServer.write("BUZZ is on");
  }
  
else if (command == "off"){
  digitalWrite(LED,LOW);   //If command is "Off", switch LED off
  Serial.println("LED is off");
  LBTServer.write("LED is off");
  
  digitalWrite(BUZZER_PIN,LOW);   //If command is "Off", switch BUZZER off
  Serial.println("BUZZ is off");
  LBTServer.write("BUZZ is off");
  }
}
