#define LED 4
void setup()
{
  //pinMode(13, OUTPUT); // choix de la LED 13
  pinMode(LED, OUTPUT);   
}

void loop() {
  digitalWrite(LED, HIGH); //Allumage de la LED
  delay(1000); //Attente de 1 seconde
  digitalWrite(LED, LOW); //Mise hors tension de la LED
  delay(1000); //Attente de 1 seconde
}
